import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemUtilRoutingModule } from './system-util-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [PageNotFoundComponent],
  imports: [
    CommonModule,
    SystemUtilRoutingModule
  ],
  exports: [PageNotFoundComponent]
})
export class SystemUtilModule { }
