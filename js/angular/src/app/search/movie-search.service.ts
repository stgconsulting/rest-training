import { BaseService } from '../service/base.service';
import { Injectable } from '@angular/core';
import { SearchResult } from './search-result.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieSearchService extends BaseService {

  constructor(protected httpClient:HttpClient) { super(httpClient); }

  // Backing store for the last loaded search results.
  private resultStore: BehaviorSubject<SearchResult> = new BehaviorSubject(new SearchResult());
  private lastQuery: string;
  private lastYear: string;
  private lastPage: number = 1;

  get results$() : Observable<SearchResult>{
    return this.resultStore.asObservable();
  }

  search(query: string, year: string): any {
      console.log("searching", query);
      this.lastPage = this.lastQuery !== query ? 1 : this.lastPage;
      this.lastQuery = query;
      this.lastYear = year;
      this.httpClient.get(MovieSearchService.BASE_URL + 'search/movie', {
                  params: new HttpParams()
                      .set('query', query)
                      .set('year', year)
                      .set('page', this.lastPage.toString())
              })
              .subscribe((res) => {
                console.log("getting search results", res);
                //this.resultStore.next(new SearchResult(res.page, res.results, res.total_results, res.total_pages)
                this.resultStore.next(res as SearchResult)});
  }

  switchPage(pageNumber: number): any {
    this.lastPage = pageNumber > 0 ? pageNumber : 1;
    this.search(this.lastQuery, this.lastYear);
  }
}
