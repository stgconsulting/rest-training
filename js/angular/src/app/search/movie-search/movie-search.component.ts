import { Component, OnInit } from '@angular/core';
import { MovieSearchService } from '../movie-search.service';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.less']
})
export class MovieSearchComponent implements OnInit {

  constructor(private searchService:MovieSearchService) { }

  ngOnInit() {
  }

}
