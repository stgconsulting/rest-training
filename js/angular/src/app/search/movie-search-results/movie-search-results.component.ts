import { Component, OnInit, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { MovieSearchService } from '../movie-search.service';
import { MovieImageService } from '../../service/movie-image.service';
import { SearchResult } from '../search-result.model';
import { Movie } from '../movie.model';
import { PageEvent } from '@angular/material/paginator';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-movie-search-results',
  templateUrl: './movie-search-results.component.html',
  styleUrls: ['./movie-search-results.component.less']
})
export class MovieSearchResultsComponent implements AfterViewInit {

  constructor(private searchService: MovieSearchService, private imageService:MovieImageService) { }

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();

  dataSource: any = new MatTableDataSource<any>();
  displayedColumns = ['poster', 'originalTitle'];
  hasResults: boolean = false;

  ngAfterViewInit() {

    this.paginator.toArray().forEach((p) => {
      p.hidePageSize=true;
      p.pageSize=20
      p.page.subscribe((e) => this.searchService.switchPage(e.pageIndex+1));
    });

    this.searchService.results$.subscribe((res) => {
      this.dataSource.data = res.results;
      this.hasResults = res.results.length > 0;
      this.paginator.toArray().forEach((p) => {
        p.length=res.total_results;
        p.pageIndex=res.page-1
      });
    });
  }

  posterUrl(path:string) : string {
    return this.imageService.image(path, 'w185');
  }


}
