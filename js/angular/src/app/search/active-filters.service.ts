import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ActiveFiltersService {

  constructor(private location:Location) { }

  filter(filters: any) {
      let path = this.location.path().substring(0,this.location.path().lastIndexOf("?"));
      let query = Object.keys(filters).reduce(function(result, key) {
        if (filters[key]) {
          result.push(key + "=" + filters[key]);
        }
        return result;
      },[]).join('&');
      this.location.go(path, query, null);
  }
}
