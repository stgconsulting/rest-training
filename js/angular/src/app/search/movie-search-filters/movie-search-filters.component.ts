import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { MovieSearchService } from '../movie-search.service';
import { ActiveFiltersService } from '../active-filters.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-search-filters',
  templateUrl: './movie-search-filters.component.html',
  styleUrls: ['./movie-search-filters.component.less']
})
export class MovieSearchFiltersComponent implements OnInit, OnDestroy {

  sub : any;

  searchForm = this.fb.group({
    search: ['', Validators.required],
    year: ['', Validators.pattern("\\d{4}")]
  });

  constructor(private fb: FormBuilder,
              private searchService: MovieSearchService,
              private activeFiltersService: ActiveFiltersService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.queryParamMap.subscribe(p => {
      let query = p.get('query');
      let year = p.get('year');

      this.searchForm.get('search').setValue(p.get('query'));
      this.searchForm.get('year').setValue(year);

      if (query || year) {
        this.onSubmit();
      }
    });
  }


  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSubmit() {
    console.warn(this.searchForm.value.search);
    this.searchService.search(this.searchForm.value.search, this.searchForm.value.year);
    this.activeFiltersService.filter({"query":this.searchForm.value.search, "year":this.searchForm.value.year});
  }
}
