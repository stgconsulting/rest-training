import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { MovieSearchComponent } from './movie-search/movie-search.component';
import { MovieSearchFiltersComponent } from './movie-search-filters/movie-search-filters.component';
import { MovieSearchResultsComponent } from './movie-search-results/movie-search-results.component';

import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [MovieSearchComponent, MovieSearchFiltersComponent, MovieSearchResultsComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,

  ],
})
export class SearchModule { }
