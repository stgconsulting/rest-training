import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieSearchComponent } from './movie-search/movie-search.component'

const routes: Routes = [
  { path: 'movie', component: MovieSearchComponent  },
  { path: '',
      redirectTo: '/search/movie',
      pathMatch: 'full',
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
