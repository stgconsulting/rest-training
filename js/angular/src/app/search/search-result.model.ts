import {Movie} from './movie.model';


export class SearchResult {

  constructor(
    public page:number = 0,
    public results:Movie[] = [],
    public total_results:number = 0,
    public total_pages:number = 0
  ) {}

}
