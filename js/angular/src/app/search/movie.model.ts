
export class Movie {

  constructor(
      poster_path: string,
      adult: boolean,
      overview:string,
      release_date: string,
      genre_ids: Number[],
      id: Number,
      original_title: string,
      original_language: string,
      title: string,
      backdrop_path: string,
      popularity: Number,
      vote_count: Number,
      video: boolean,
      vote_average: Number
    ){};
}
