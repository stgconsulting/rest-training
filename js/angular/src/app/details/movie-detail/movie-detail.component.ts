import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MovieDetail } from '../movie-detail.model';
import { MovieDetailService } from '../movie-detail.service';
import { MovieImageService } from '../../service/movie-image.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.less']
})
export class MovieDetailComponent implements OnInit{

  movie$ : Observable<MovieDetail>;

  companyCount: number;

  constructor(private detailService:MovieDetailService,
              private imageService:MovieImageService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.movie$ = this.detailService.results$;
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        of(params.get('movieId'))
      )
    ).subscribe((id) => {
      this.detailService.loadDetail(id);
    });
  }

  backgroundUrl(path) : string {
    return this.imageService.image(path, 'w300');
  }

  posterUrl(path:string) : string {
    return this.imageService.image(path, 'w185');
  }

  logoUrl(path:string) : string {
    return this.imageService.image(path, 'w45');
  }

}
