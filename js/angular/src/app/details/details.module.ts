import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MatGridListModule } from '@angular/material/grid-list';

import { DetailsRoutingModule } from './details-routing.module';

@NgModule({
  declarations: [MovieDetailComponent],
  imports: [
    CommonModule,
    DetailsRoutingModule,
    MatGridListModule
  ]
})
export class DetailsModule { }
