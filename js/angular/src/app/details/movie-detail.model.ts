
export class MovieDetail {

  constructor(
     public adult:boolean,
     public backdrop_path:string,
     public belongs_to_collection:any,
     public budget:Number,
     public genres:Genre[],
     public homepage:string,
     public id:Number,
     public imdb_id:string,
     public original_language:string,
     public original_title:string,
     public overview,string,
     public popularity:Number,
     public poster_path:string,
     public production_companies:ProductionCompany[],
     public release_date:string,
     public revenue:Number,
     public runtime:Number,
     public spoken_languages:SpokenLanguage[],
     //Allowed Values: Rumored, Planned, In Production, Post Production, Released, Canceled
     public status:string,
     public tagline:string,
     public title:string,
     public video:boolean,
     public vote_average:Number,
     public vote_count:Number
    ){};
}

export class Genre {
  constructor(id:Number, name:string){};
}

export class ProductionCompany {
  constructor(name:string, id:Number,logo_path:string,origin_country:string){};
}

export class ProductionCountry {
  constructor(iso_3166_1:string, name:string) {};
}

export class SpokenLanguage {
  constructor(iso_639_1:string, name:string){};
}

