import { BaseService } from '../service/base.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { MovieDetail } from './movie-detail.model';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailService extends BaseService {

  private resultStore: BehaviorSubject<MovieDetail> = new BehaviorSubject({} as MovieDetail);

  get results$() : Observable<MovieDetail> {
    return this.resultStore.asObservable();
  }
  constructor(protected httpClient:HttpClient) { super(httpClient); }

  loadDetail(id: string): any {
      console.log("loading", id);
      this.httpClient.get<MovieDetail>(MovieDetailService.BASE_URL + 'movie/' + id)
        .subscribe(res => this.resultStore.next(res));
  }

}
