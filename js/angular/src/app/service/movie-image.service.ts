import { Injectable } from '@angular/core';
import { BaseService } from '../service/base.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieImageService extends BaseService {

  private config:any;

  constructor(protected httpClient: HttpClient) {
    super(httpClient);

    this.init();
  }

  private init() {
    this.httpClient.get(MovieImageService.BASE_URL + 'configuration', {})
      .subscribe( (data) => this.config = data);
  }

  image(path:string, size:string = 'original'): string {
    if (!this.config) {
      return "";
    }
    return this.config.images.base_url + '/' + size + '/' + path;
  }
}
