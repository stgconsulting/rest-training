import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MovieImageService } from './movie-image.service';

describe('MovieImageService', () => {

  let httpMock: HttpTestingController;

  beforeEach(() => {
     TestBed.configureTestingModule({
       imports: [HttpClientTestingModule],
       providers: [],
     });

     httpMock = TestBed.get(HttpTestingController);
   });

  it('should be created', () => {
    const service: MovieImageService = TestBed.get(MovieImageService);
    expect(service).toBeTruthy();
  });
});
