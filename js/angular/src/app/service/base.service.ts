import { HttpClient } from '@angular/common/http';

export class BaseService {

  static readonly BASE_URL = 'https://api.themoviedb.org/3/';

  protected constructor(protected httpClient:HttpClient) {}
}
