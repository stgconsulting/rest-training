import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemUtilModule } from './system-util/system-util.module';
import { PageNotFoundComponent } from './system-util/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'search',
    loadChildren: './search/search.module#SearchModule'
  },
  { path: 'details',
    loadChildren: './details/details.module#DetailsModule'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    SystemUtilModule,
    RouterModule.forRoot(routes, {
      enableTracing: true, // <-- debugging purposes only
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
