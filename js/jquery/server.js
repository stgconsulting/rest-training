var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

app.use(express.static('.'));

app.use(express.json());

app.get('/', function(req, res){
    res.sendfile('index.html');
});

app.post('/post', function (req, res) {
  let body = req.body;
  console.log(body);

  if (body.error) {
    res = res.status(body.error) || '200';
  }

  Object.entries(body).forEach(([key, value]) => {
     body[key] = value + " " + value + " " + value;
  })

  if (req.get('Content-Type')) {
    console.log("Content-Type: " + req.get('Content-Type'));
    res = res.type(req.get('Content-Type'));
  }
  res.send(body);
});


app.listen(port);

console.log('Server started on: ' + port);

