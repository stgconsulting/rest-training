# MovieInfo App

JavaScript/jQuery app that fetches movie data from The Movie Database (TMDb) API

**Modified code from: https://github.com/bradtraversy/movieinfo.git**

## Usage

In order to run this project, you need a key from The Movie DB.  See [here](https://developers.themoviedb.org/3/getting-started/authentication) for how to obtain an `api_key`.

When you have the key, include it in the [apiKey.js](./apiKey.js) file.

Install Node ***and*** NPM : https://nodejs.org/en/download/

Initialize the project
```bash
npm install
```

Start the host server
```bash
npm run start
```

Open a browser to [http://localhost:3000]