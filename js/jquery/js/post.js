$(document).ready(() => {

  $('#echoForm').on('submit', (e) => {
    let echoText = $('#echoText').val();
    let error = $('#errorCode').val() || "200";
    console.log(echoText, error);
    echo(echoText, error);
    e.preventDefault();
  });
});

const echoApi = axios.create({baseURL: 'http://localhost:3000/'});

function echo(echoText, errorCode){
  console.log("echo", echoText, "code", errorCode)
  echoApi.post('post', { echo: echoText, error: errorCode })
    .then((response) => {
      console.log(response.data.echo);

      $('#echo').html(response.data.echo);
    })
    .catch((err) => {
      console.log(err.response);
      $('#echo').html("Error : " + err.response.statusText + " Sent:" +  err.response.data.error);
    });
}

