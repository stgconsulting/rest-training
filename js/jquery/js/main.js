$(document).ready(() => {

  configure();

  $('#searchForm').on('submit', (e) => {
    let searchText = $('#searchText').val();
    getMovies(searchText);
    e.preventDefault();
  });
});

const movieApi = axios.create();

function configure() {

  movieApi.defaults.baseURL = "https://api.themoviedb.org/3/";

  movieApi.interceptors.request.use(function (config) {
    console.log("config", config);
    let key = 'api_key=' + API_KEY;
    key = (config.url.indexOf('?') >= 0 ? "&" : '?') + key;
    config.url += key;
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

  movieApi.get('configuration').then((response) => {
    movieApi.configuration = response.data;
  });
}

function getMovies(searchText){
  movieApi.get('search/movie?query='+searchText)
    .then((response) => {
      console.log(response);
      let movies = response.data.results;
      let output = '';
      $.each(movies, (index, movie) => {
        output += `
          <div class="col-md-3">
            <div class="well text-center">
              <img src="${posterUrl(movie.poster_path)}">
              <h5>${movie.title}</h5>
              <a onclick="movieSelected('${movie.id}')" class="btn btn-primary" href="#">Movie Details</a>
            </div>
          </div>
        `;
      });

      $('#movies').html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

function movieSelected(id){
  sessionStorage.setItem('movieId', id);
  window.location = 'movie.html';
  return false;
}

function getMovie(){
  let movieId = sessionStorage.getItem('movieId');

  movieApi.get('movie/'+movieId)
    .then((response) => {
      console.log(response);
      let movie = response.data;
      let genres = movie.genres.map((g) => {return g.name;}).join(",");
      let productionCompanies = movie.production_companies.map((c) => { return c.name;}).join(",");
      let output =`
        <div class="row">
          <div class="col-md-4">
            <img src="${posterUrl(movie.poster_path)}" class="thumbnail">
          </div>
          <div class="col-md-8">
            <h2>${movie.title}</h2>
            <h4>${movie.tagline}</h4>
            <ul class="list-group">
              <li class="list-group-item"><strong>Genre:</strong> ${genres}</li>
              <li class="list-group-item"><strong>Released:</strong> ${movie.release_date}</li>
              <li class="list-group-item"><strong>Runtime:</strong> ${movie.runtime}</li>
              <li class="list-group-item"><strong>Production Companies:</strong> ${productionCompanies}</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="well">
            <h3>Plot</h3>
            ${movie.overview}
            <hr>
            <a href="http://imdb.com/title/${movie.imdb_id}" target="_blank" class="btn btn-primary">View IMDB</a>
            <a href="index.html" class="btn btn-default">Go Back To Search</a>
          </div>
        </div>
      `;

      $('#movie').html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

function posterUrl(path) {
  return movieApi.configuration.images.secure_base_url + movieApi.configuration.images.poster_sizes[1] +"/" + path;
}


