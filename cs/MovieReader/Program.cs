﻿using MovieReaderLib.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReader
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            await GetLists();

            //await GetMovieDetails(500);

            //await GetMovieTitles(500);

            //await SetRating(500, 8);

            //await DeleteRating(500);
        }

        static async Task GetLists()
        {
            var lib = new MovieReaderLib.Lists("https://api.themoviedb.org/3/");

            var mgenres = await lib.GetMovieGenres();
            var tgenres = await lib.GetTVGenres();

            foreach (var g in mgenres)
            {
                Console.WriteLine($"{g.id} - {g.name}");
            }

            Console.WriteLine("***TV***");
            foreach (var g in tgenres)
            {
                Console.WriteLine($"{g.id} - {g.name}");
            }

            var popShows = await lib.GetPopularTVShows();
            foreach(var t in popShows)
            {
                Console.WriteLine($"{t.id} - {t.name}");
            }
        }

        static async Task GetMovieDetails(int movieId)
        {
            var movies = new MovieReaderLib.Movies("https://api.themoviedb.org/3/movie/", movieId);
            var details = await movies.GetDetailsAsync();

            Console.WriteLine(JsonConvert.SerializeObject(details).ToString());
        }

        static async Task GetMovieTitles(int movieId)
        {
            var movies = new MovieReaderLib.Movies("https://api.themoviedb.org/3/movie/", movieId);
            var titles = await movies.GetAlternativeTitles();

            Console.WriteLine(JsonConvert.SerializeObject(titles).ToString());

        }

        static async Task SetRating(int movieId, double newRating)
        {
            var movies = new MovieReaderLib.Movies("https://api.themoviedb.org/3/movie/", movieId);
            var response = await movies.SetRating(newRating);

            Console.WriteLine(response.IsSuccessStatusCode);

        }

        static async Task DeleteRating(int movieId)
        {
            var movies = new MovieReaderLib.Movies("https://api.themoviedb.org/3/movie/", movieId);
            var response = await movies.DeleteRating();

            Console.WriteLine(response.IsSuccessStatusCode);

        }
    }
}
