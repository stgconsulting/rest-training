﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MovieReaderLib
{
    public class MovieDbBase
    {
        protected HttpClient _client;
        protected string _apiKey = "?api_key=52a2c3f551a531ddc234b86c6b54c751";
        protected string _guestSessionId = "&guest_session_id=7a969f580755cca2225826d5cfb7d2d0";
        protected string _baseUrl = "";

        public MovieDbBase(string baseUrl)
        {
            _baseUrl = baseUrl;
            //Instantiate a new instance of the HttpClient.  This will use the
            //base part of the API url.
            _client = new HttpClient
            {
                BaseAddress = new Uri(_baseUrl)
            };

            //Make sure there are no headers on the client.  This is so we can control
            //which headers will be included
            _client.DefaultRequestHeaders.Accept.Clear();

            //Since we will be using json, we will add the accept header for that
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //If we were using JWT, we would add that here
            //client.DefaultRequestHeaders.Accept.Add(new AuthenticationHeaderValue("Bearer", TOKEN))

        }

        protected async Task<HttpResponseMessage> GetData(string urlSuffix)
        {
            var response = await _client.GetAsync($"{urlSuffix}{_apiKey}");
            return response;
        }

        protected async Task<HttpResponseMessage> PostData(string urlSuffix, HttpContent content)
        {
            var response = await _client.PostAsync($"{urlSuffix}{_apiKey}{_guestSessionId}", content);
            return response;
        }

        protected async Task<HttpResponseMessage> DeleteData(string urlSuffix)
        {
            var response = await _client.DeleteAsync($"{urlSuffix}{_apiKey}{_guestSessionId}");
            return response;
        }
    }
}
