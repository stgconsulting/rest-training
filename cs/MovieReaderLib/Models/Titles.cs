﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReaderLib.Models
{
    public class Title
    {
        public string iso_3166_1 { get; set; }
        public string title { get; set; }
        public string type { get; set; }
    }

    public class Titles
    {
        public int id { get; set; }
        public List<Title> titles { get; set; }
    }
}
