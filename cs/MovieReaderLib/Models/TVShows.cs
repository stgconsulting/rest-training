﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReaderLib.Models
{
    public class TVShow
    {
        public string original_name { get; set; }
        public List<int> genre_ids { get; set; }
        public string name { get; set; }
        public double popularity { get; set; }
        public List<object> origin_country { get; set; }
        public int vote_count { get; set; }
        public string first_air_date { get; set; }
        public string backdrop_path { get; set; }
        public string original_language { get; set; }
        public int id { get; set; }
        public double vote_average { get; set; }
        public string overview { get; set; }
        public string poster_path { get; set; }
    }

    public class PopularTVShows
    {
        public int page { get; set; }
        public int total_results { get; set; }
        public int total_pages { get; set; }
        public List<TVShow> results { get; set; }
    }
}
