﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReaderLib.Models
{
    public class Genre
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class GenreList
    {
        public List<Genre> genres { get; set; }
    }
}
