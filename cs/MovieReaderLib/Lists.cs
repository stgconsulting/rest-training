﻿using MovieReaderLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MovieReaderLib
{
    public class Lists  : MovieDbBase
    {
        public Lists(string baseUrl) : base(baseUrl)
        {
            
        }
        public async Task<List<Genre>> GetMovieGenres() {
            var genrePath = $"genre/movie/list{_apiKey}";
            return await GetGenres(genrePath);
        }

        public async Task<List<Genre>> GetGenres(string genrePath) { 
            GenreList genres  = null;
            var response = await _client.GetAsync(genrePath);
            if (response.IsSuccessStatusCode)
            {
                genres = await response.Content.ReadAsAsync<GenreList>();
            }
            return genres.genres.OrderBy(o => o.id).ToList();
        }

        public async Task<List<Genre>> GetTVGenres()
        {
            var genrePath = $"genre/tv/list{_apiKey}&language=en-US";
            return await GetGenres(genrePath);
        }

        public async Task<List<TVShow>> GetPopularTVShows()
        {
            var apiPath = $"tv/popular{_apiKey}&language=en-US";
            PopularTVShows shows = null;
            var response = await _client.GetAsync(apiPath);
            if (response.IsSuccessStatusCode)
            {
                shows = await response.Content.ReadAsAsync<PopularTVShows>();
                return shows.results.OrderBy(o => o.name).ToList();
            }
            return null;
        }
    }
}
