﻿using MovieReaderLib.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MovieReaderLib
{
    public class Movies : MovieDbBase
    {
        public Movies(string baseUrl, int movieId) : base($"{baseUrl}{movieId}/")
        {
            
        }

        public async Task<MovieDetails> GetDetailsAsync()
        {
            MovieDetails details = null;

            var response = await GetData("");
            if (response.IsSuccessStatusCode)
            {
                details = await response.Content.ReadAsAsync<MovieDetails>();
            }

            return details;
        }

        public async Task<Titles> GetAlternativeTitles()
        {
            Titles titles = null;

            var response = await GetData("alternative_titles"); 
            if (response.IsSuccessStatusCode)
            {
                titles = await response.Content.ReadAsAsync<Titles>();
            }

            return titles;
        }

        public async Task<HttpResponseMessage> SetRating(double newRating)
        {
            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("value", newRating.ToString())
                });
            var response = await PostData("rating", content);
            return response;
        }

        public async Task<HttpResponseMessage> DeleteRating()
        {
            var response = await DeleteData("rating");
            return response;
        }
    }
}
